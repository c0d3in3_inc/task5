package com.c0d3in3.task5.model

class SongsModel {
    var band = ""
    var songs = ArrayList<SongModel>()

    inner class SongModel(val title : String)
}
