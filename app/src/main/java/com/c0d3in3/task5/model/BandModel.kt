package com.c0d3in3.task5.model

import com.google.gson.annotations.SerializedName

class BandModel {
    var name = ""
    @SerializedName("img_url")
    var imgUrl = ""
    var thumb1 = ""
    var thumb2 = ""
    var thumb3 = ""
    var info = ""
    var genre = ""
}