package com.c0d3in3.task5.lyrics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.c0d3in3.task5.BaseFragment
import com.c0d3in3.task5.MainActivity
import com.c0d3in3.task5.R
import com.c0d3in3.task5.api.ApiCallback
import com.c0d3in3.task5.api.ApiHandler
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_lyrics.view.*
import org.json.JSONObject

class LyricsFragment(private val band : String, private val title : String) : BaseFragment() {



    private fun getLyrics(){
        ApiHandler.getRequest("LYRICS", "$band/$title", object : ApiCallback{
            override fun onSuccess(response: String, code: Int) {
                val json = JSONObject(response)
                val lyrics = json.get("lyrics")
                rootView!!.lyricsTextView.text = lyrics.toString()
            }

            override fun onError(response: String, code: Int) {
                rootView!!.lyricsTextView.text = response
            }
        })
    }

    override fun init() {
        (activity as MainActivity).changeHeaderTag(band)
        rootView!!.bandNameTextView.text = title
        getLyrics()
    }

    override fun getLayout() = R.layout.fragment_lyrics
}