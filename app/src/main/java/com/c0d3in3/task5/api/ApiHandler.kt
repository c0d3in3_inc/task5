package com.c0d3in3.task5.api

import okhttp3.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


object ApiHandler {

    private val httpClient = OkHttpClient.Builder()
        .build();

    private var retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BANDS_URL)
        .client(httpClient)
        .build()

    private var service: NetworkService = retrofit.create(NetworkService::class.java)

    private var lyrics: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(LYRICS_URL)
        .client(httpClient)
        .build()

    private var lyricsService: NetworkService = lyrics.create(NetworkService::class.java)

    interface NetworkService {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): Call<String>

        @GET
        fun getLyrics(@Url path: String): Call<String>
    }


    fun getRequest(client : String, path : String, callback : ApiCallback) {
        if(client == "BANDS"){
            val call = service.getRequest(path)
            call.enqueue(onCallback(callback))
        }
        else{
            val call = lyricsService.getLyrics(path)
            call.enqueue(onCallback(callback))
        }

    }


    private fun onCallback(callback: ApiCallback): Callback<String> =
        object : Callback<String>{
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val status = response.code()
                if(status == HTTP_200_OK || status == HTTP_201_CREATED) callback.onSuccess(response.body().toString(), status)
                else if(status == HTTP_400_BAD_REQUEST) callback.onError("Bad request", status)
                else if(status == HTTP_204_NO_CONTENT) callback.onError("No content", status)
                else if(status == HTTP_401_UNAUTHORIZED) callback.onError("Unauthorized please log in again", status)
                else if(status == HTTP_404_NOT_FOUND) callback.onError("Resource not found", status)
                else if(status == HTTP_500_INTERNAL_SERVER_ERROR) callback.onError("Internal server error", status)
            }
            override fun onFailure(call: Call<String>, t: Throwable) {
                callback.onFail(t.toString())
            }
        }
}