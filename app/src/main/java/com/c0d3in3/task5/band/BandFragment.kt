package com.c0d3in3.task5.band

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.c0d3in3.task5.BaseFragment
import com.c0d3in3.task5.MainActivity
import com.c0d3in3.task5.R
import com.c0d3in3.task5.adapter.BandsAdapter
import com.c0d3in3.task5.adapter.SongsAdapter
import com.c0d3in3.task5.api.ApiCallback
import com.c0d3in3.task5.api.ApiHandler
import com.c0d3in3.task5.api.SONGS
import com.c0d3in3.task5.lyrics.LyricsFragment
import com.c0d3in3.task5.model.BandModel
import com.c0d3in3.task5.model.SongsModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_band_info.view.*
import org.json.JSONArray
import org.json.JSONObject

class BandFragment(val model : BandModel) : BaseFragment(), SongsAdapter.CustomCallback {

    private val songsList = arrayListOf<SongsModel.SongModel>()
    private val adapter = SongsAdapter(model.name, songsList, this)


    private fun getSongs(){
        ApiHandler.getRequest("BANDS", SONGS, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val json = JSONObject(response)
                val songsArray = json.getJSONArray("data")
                for(i in 0 until songsArray.length()) {
                    val songs = songsArray.getJSONObject(i)
                    if(songs.getString("band") == model.name){
                        val titles = songs.getJSONArray("songs")
                        for(j in 0 until titles.length()){
                            val songModel = Gson().fromJson(titles[j].toString(), SongsModel.SongModel::class.java)
                            songsList.add(songModel)
                            adapter.notifyItemInserted(songsList.size - 1)
                        }
                    }
                }
            }
        })
    }


    override fun showLyrics(band: String, title: String) {
        val transaction = (activity as MainActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, LyricsFragment(band, title))
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun init() {
        (activity as MainActivity).changeHeaderTag(model.name)
        rootView!!.songsRecyclerView.layoutManager = LinearLayoutManager(context)
        rootView!!.songsRecyclerView.adapter = adapter
        Glide
            .with(context!!)
            .load(model.imgUrl)
            .centerCrop()
            .into(rootView!!.bandImageView)
        rootView!!.bandInfoTextView.text = model.info
        getSongs()
    }

    override fun getLayout() = R.layout.fragment_band_info
}