package com.c0d3in3.task5.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.c0d3in3.task5.R
import com.c0d3in3.task5.band.BandFragment
import com.c0d3in3.task5.model.BandModel
import kotlinx.android.synthetic.main.band_item_layout.view.*

class BandsAdapter(private val items: ArrayList<BandModel>, private val listener: CustomCallback) : RecyclerView.Adapter<BandsAdapter.ViewHolder>() {

    interface CustomCallback{
        fun showInfo(position: Int)
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val model = items[adapterPosition]
            Glide
                .with(itemView.context)
                .load(model.imgUrl)
                .centerCrop()
                .into(itemView.bandImage)

            itemView.bandName.text = model.name
            when (model.genre) {
                "rock" -> itemView.bandGenre.setBackgroundColor(Color.GREEN)
                "Heavy Metal" -> itemView.bandGenre.setBackgroundColor(Color.RED)
                "hard rock"-> itemView.bandGenre.setBackgroundColor(Color.YELLOW)
                " hard rock"-> itemView.bandGenre.setBackgroundColor(Color.YELLOW)
                "grunge" -> itemView.bandGenre.setBackgroundColor(Color.BLUE)
            }
            itemView.bandGenre.text = model.genre

            itemView.setOnClickListener {
                listener.showInfo(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.band_item_layout, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}