package com.c0d3in3.task5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.task5.R
import com.c0d3in3.task5.model.SongsModel
import kotlinx.android.synthetic.main.band_songs_layout.view.*

class SongsAdapter(private val band : String, private val items: ArrayList<SongsModel.SongModel>, private val listener: CustomCallback) : RecyclerView.Adapter<SongsAdapter.ViewHolder>() {

    interface CustomCallback{
        fun showLyrics(band : String, title : String)
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val model = items[adapterPosition]
            itemView.songName.text = "${adapterPosition+1}. ${model.title}"

            itemView.setOnClickListener {
                listener.showLyrics(band, model.title)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.band_songs_layout, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}