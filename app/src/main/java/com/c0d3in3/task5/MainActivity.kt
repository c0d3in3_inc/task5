package com.c0d3in3.task5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.GridLayoutManager
import com.c0d3in3.task5.api.ApiCallback
import com.c0d3in3.task5.api.ApiHandler
import com.c0d3in3.task5.api.BANDS
import com.c0d3in3.task5.api.SONGS
import com.c0d3in3.task5.dashboard.DashboardFragment
import com.c0d3in3.task5.model.BandModel
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, DashboardFragment())
        transaction.commit()

    }

    fun changeHeaderTag(title : String){
        headerTag.text = title
    }
}
