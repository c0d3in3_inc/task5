package com.c0d3in3.task5.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.c0d3in3.task5.BaseFragment
import com.c0d3in3.task5.MainActivity
import com.c0d3in3.task5.R
import com.c0d3in3.task5.adapter.BandsAdapter
import com.c0d3in3.task5.api.ApiCallback
import com.c0d3in3.task5.api.ApiHandler
import com.c0d3in3.task5.api.BANDS
import com.c0d3in3.task5.band.BandFragment
import com.c0d3in3.task5.model.BandModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import org.json.JSONArray

class DashboardFragment : BaseFragment(), BandsAdapter.CustomCallback{

    val bandsList = arrayListOf<BandModel>()
    private val adapter = BandsAdapter(bandsList, this)

    override fun init() {
        (activity as MainActivity).changeHeaderTag("Dashboard")
        rootView!!.bandsRecyclerView.layoutManager = GridLayoutManager(context, 2)
        rootView!!.bandsRecyclerView.adapter = adapter
        bandsList.clear()
        getBands()
    }


    override fun getLayout() = R.layout.fragment_dashboard

    private fun getBands(){
        ApiHandler.getRequest("BANDS", BANDS, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val json = JSONArray(response)
                for(i in 0 until json.length()) {
                    val bandsModel = Gson().fromJson(json[i].toString(), BandModel::class.java)
                    bandsList.add(bandsModel)
                    adapter.notifyItemInserted(bandsList.size-1)
                }
            }
        })
    }

    override fun showInfo(position: Int) {
        val transaction = (activity as MainActivity).supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, BandFragment(bandsList[position]))
        transaction.addToBackStack(null)
        transaction.commit()
    }
}